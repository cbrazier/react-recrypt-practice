export const setStorage = async(item, name) => window.localStorage.setItem(name, JSON.stringify(item));

export const getStorage = (name) => window.localStorage.getItem(name);

export const clearStorage = () => window.localStorage.clear();
