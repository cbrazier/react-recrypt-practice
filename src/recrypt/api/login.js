import { Password } from "../steganography/password";
import { AES } from "../steganography/aes";
import { setStorage } from "../storage";

const RecryptLogin = async (loginDetails) => {
  const url = "http://localhost:3000/users/sign_in.json";
  const prehashPass = await Password.prehash(loginDetails.email, loginDetails.password);
  const body = JSON.stringify({
    user: {
      email: loginDetails.email,
      password: prehashPass
    }
  });

  const response = await fetch(url, 
    { 
      method: "POST",
      mode: 'cors',
      body: body,
      headers: {
        'Content-Type': 'application/json'
      },
      credentials: 'omit'
    }
  );

  const key = await Password.masterKey(loginDetails.email, loginDetails.password);
  const rawKey = await AES.exportKey(key);
  const json = await response.json();
  json["key"] = rawKey;
  setStorage(json, "token");

  return {response, key, json};
 }

 export default RecryptLogin;