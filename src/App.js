import React, { Fragment, Component } from "react";
import { BrowserRouter as Router,
  Switch, 
  Route,
  Redirect
} from "react-router-dom";
import Account from "./components/Nav/Account";
import Title from "./components/Nav/Title";
import Nav from "./components/Nav/Nav"
import Footer from "./components/Nav/Footer";
// import Home from "./components/Pages/Home";
// import Login from "./components/User/Login";
import Secrets from "./components/Secrets/Secrets";
import RecryptLogin from "./recrypt/api/login";
import { getStorage } from "./recrypt/storage";
// import { Logout } from "./components/User/Logout";
// import { AccountInfoContext } from "./context/index";
import styled from 'styled-components';

const AppContent = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  background: #252A41 0% 0% no-repeat padding-box;
`
const PageContent = styled.div`
  margin-bottom: 12.75rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  flex: 1;
`;

class App extends Component {

  // state = {
  //   accountInfo: JSON.parse(getStorage("token")),
  // };

  // getAccountInfo = info => {
  //   RecryptLogin(info).then(response => {
  //     const status = response["response"].status;
  //     if(status === 200){
  //       this.setState({ accountInfo: response["json"]})
  //     }
  //   });
  // }

  // logout = () => {
  //   this.setState({ accountInfo: null });
  // }

  render(){
    return (
      <Router>
        <AppContent>
          <Account />
          <PageContent>
            <Title />
            <Nav/>
            <Switch>
              <Route exact path="/">
                <Secrets />
              </Route>
            </Switch>
          </PageContent>
          <Footer />
        </AppContent>
      </Router>
      // <Fragment>
      //   <AccountInfoContext.Provider value={{ accountInfo: this.state.accountInfo, logout: this.logout}} >
      //     <Router>
      //       <Navbar />
      //       <Switch>
      //         <Route exact path="/">
      //           {/* <Home /> */}
      //         </Route>
      //         <Route path="/secrets">
      //           <Secrets />
      //         </Route>
      //         <Route path="/login">
      //           { !!this.state.accountInfo ? <Redirect to={{ pathname: '/', state: 'You are Logged In!'}}/> : <Login getAccountInfo={this.getAccountInfo} />}
      //         </Route>
      //         <Route path="/logout">
      //           <Logout logout={this.logout}/>
      //         </Route>
      //       </Switch>
      //     </Router>
      //   </AccountInfoContext.Provider>
      // </Fragment>
    );
  }
}

export default App;
