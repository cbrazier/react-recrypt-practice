import React from 'react';

const AccountInfoContext = React.createContext({
  accountInfo: null,
  logout: () => {}
});

export { AccountInfoContext };