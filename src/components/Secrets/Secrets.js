import React, { useState } from "react";
import styled from "styled-components";
import {CSSTransition} from "react-transition-group";
import SearchBar from "./SearchBar";

const SecretList = styled.div`

  width: 50%;
  height: 400px;

  background: #3B4263 0% 0% no-repeat padding-box;
  border-radius: 5px 5px 0px 0px;
  
  display: flex;
  flex: 1;
  justify-content: space-between;
  align-items: center;
  
`
const ListTransition = styled.div`

  .content-enter {
    max-height: 0;
    overflow: hidden;
  }

  .content-enter-active {
    max-height: 400px;
    transition: max-height .2s ease-in;

  }

  .content-exit {
    max-height: 400px;

  }

  .content-exit-active {
    max-height: 0;
    transition: max-height .2s ease-out;
    overflow: hidden;
  }
`

const DisplayButton = styled.div`
  width: 50%;
  display: flex;
  justify-content: space-between;
  align-items: center;

  background: #343B5A 0% 0% no-repeat padding-box;
  border-radius: 5px;
  padding-top: 0.5rem;

  > a  {
    text-decoration: none;
    margin-left: .5rem;
    margin-bottom: .5rem;
    font-size: 14px;
    text-align: left;
    color: #98A2BB;

    > i {
      padding-right: 5px;
    }
  }

  > p {
    margin-right: .5rem;
    margin-bottom: 1rem;
    font-size: 12px;
    text-align: right;
    color: #98A2BB;
  }
`

const Secrets = () => {

  const [ display, setDisplay ] = useState(false);

  return (
    <div align="center">
      <SearchBar />
      
      <ListTransition>
        <CSSTransition
             classNames="content"
             in={display}
            timeout={200}
            unmountOnExit
        > 
          <SecretList></SecretList> 
        </CSSTransition>
        <DisplayButton>
          {!!display ? 
            <>
            <a href="#" onClick={() => setDisplay(!display)}><i class="fas fa-plus-circle"/>Add a Secret to Recrypt</a>
            <p>Shift + N</p>
            </>
            :
            <> 
            <a href="#" onClick={() => setDisplay(!display)}><i class="fa fa-arrow-alt-circle-down"/>See all secrets</a>
            <p>Shift + <i class="fa fa-caret-square-down"/></p>
            </> 
          }
        </DisplayButton>
      </ListTransition>

    </div>
  );
}

export default Secrets;