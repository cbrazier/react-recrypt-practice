import React from "react";
import styled from "styled-components";

const SecretSearch = styled.div`

  display: flex;
  justify-content: center;

  > input {
    text-indent: 10px;
    height: 3rem;
    width: 50%;
    box-sizing: border-box;
    color: #BAC2D7;
    background: none;
    border: none;
    border-radius: 5px;
    background: #4C5374 0% 0% no-repeat padding-box;

    ::placeholder{
      opacity: .5;
      color: #BAC2D7;
    }
  }
`

const SearchBar = () => (
  <SecretSearch>
    <input type="text" placeholder="Search for a secret" />
  </SecretSearch>
)

export default SearchBar;
