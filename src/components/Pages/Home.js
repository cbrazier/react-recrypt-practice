import React from "react";
import { useLocation } from "react-router-dom";
import Message from "../Notifications/Message";
import "../../styles/Pages/home.css";

const Home = () =>{

  const location = useLocation();

  return (
    <div className="home-container">
      <div className="home-title">
        <h2>Home</h2>
      </div>
      <Message location={location} />
      <div className="body">
      </div>
    </div>
  );
}

export default Home;

