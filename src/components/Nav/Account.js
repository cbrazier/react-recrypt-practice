import React from "react";
import { Link } from "react-router-dom";
import styled from 'styled-components';

const AccountBar = styled.div`

  width: 100%;
  padding: 30px 0px 30px 0px;

  /* background-color: pink; */

  display: flex;
  flex-direction: row;
  justify-content: flex-end;

  > a {
    display: flex;
    align-items: center;

    margin-right: 3rem;
    text-decoration: none;
    color: #98A2BB;

    &:hover {
      color: white;
    }

    >i {  
      padding-left: .5rem;
      font-size: 25px;
    }
  }
`
const Account = () => (
  <AccountBar>
    <Link to="/admin">Admin Area</Link>
    <Link to="/user">Users Name <i class="fa fa-user-circle"/></Link>
  </AccountBar>
)

export default Account;