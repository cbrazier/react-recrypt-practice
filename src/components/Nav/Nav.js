import React from 'react'
import { Link } from "react-router-dom";
import styled from 'styled-components';

const NavContent = styled.div`

  width: 50%;
  height: 5rem;
  margin: 0 auto;

  /* background-color: lawngreen; */
  
  display: flex;
  justify-content: space-between;
  align-items: center;

  .Navbar {
    /* background-color: yellow; */
    
    > a {
      color: #98A2BB;
      text-decoration: none;
      padding-right: 1.5rem;
      
      &:hover {
        color: white;
      }
    }
  }

  .add-secret {
    /* background-color: darksalmon; */
    
    > p {
      color: #98A2BB;
      padding-right: 1rem;

      > i {
        padding-right: 5px;
      }
    }
  }

  
`;

const Nav = () => {
  return (
    <NavContent>
      <div className="Navbar">
        <Link to="/">Secrets</Link>
        <Link to="/chests">Chests</Link>
        <Link to="/mysecrets">My Secrets</Link>
      </div>
      <div className="add-secret">
        <p><i class="fas fa-plus-circle"/>Add a Secret</p>
      </div>
    </NavContent>
  )
}

export default Nav;