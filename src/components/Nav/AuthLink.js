import React from "react";
import { Link } from "react-router-dom";
import { AccountInfoContext } from "../../context/index";

export const AuthLink = () => (
  <AccountInfoContext.Consumer>
    {({accountInfo}) => (
         !accountInfo ? <Link to="/login">Login</Link> : <Link to="/logout">Logout</Link> 
      )}
  </AccountInfoContext.Consumer>
)
