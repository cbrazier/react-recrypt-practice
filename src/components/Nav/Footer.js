import React from 'react'
import styled from "styled-components";

const FooterBar = styled.div`

  width: 100%;
  position: fixed;
  bottom: 0;
  display: flex;
  justify-content: center;

  > p {
    color: #98A2BB;
  }

`

const Footer = () => {
  return (
    <FooterBar>
      <p>© Copyright 2020 Recrypt</p>
    </FooterBar>
  )
}

export default Footer;
