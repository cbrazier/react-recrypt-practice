import React from "react";
import { Link } from "react-router-dom";
import styled from 'styled-components';

const TitleBar = styled.div`

  width: 100%;

  /* background-color: skyblue; */

  display: flex;
  justify-content: flex-end;
  flex-direction: column;
  align-items: center;

  > a {
    text-decoration: none;
    color: #98A2BB;
    font-size: 40px;
    font-weight: bolder;

    > i {
      padding-right: 10px;
    }
  }
`
const Title = () => (
  <TitleBar>
    <Link to="/"><i class="fas fa-flag-usa"/>Recrypt</Link>
  </TitleBar>
)

export default Title;