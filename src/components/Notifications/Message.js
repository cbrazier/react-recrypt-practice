import React from 'react'
import "../../styles/Notifications/message.css";

const Message = ({ location }) => (
  !!location.state && <p className="message-banner">{location.state}</p>
);

export default Message;
