import React from "react";
import { clearStorage } from "../../recrypt/storage";
import { Redirect } from "react-router-dom";

export const Logout = ({ logout }) => {
  clearStorage();
  logout();
  return (
    <Redirect to={{ pathname: "/login", state: "You are Logged out!" }}/>
  );
}