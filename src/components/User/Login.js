import React from "react";
import Message from "../Notifications/Message";
import { useLocation } from "react-router-dom";
import "../../styles/User/login.css";

const Login = ({ getAccountInfo }) => {

  const location = useLocation();
  const emailRef = React.createRef();
  const passwordRef = React.createRef();

  const getDetails = (event) => {
    event.preventDefault();
    const loginDetails = {
      email: (emailRef.current.value).toLowerCase(),
      password: (passwordRef.current.value).toLowerCase()
    }
    getAccountInfo(loginDetails);
  }

  return (
    <div className="login-container">
      <div className="title">
        <h2>Login</h2>
      </div>
      <Message location={location} />
      <div className="body">
        <form onSubmit={getDetails}>
          <p>Email</p>
          <input name="Email" ref={emailRef} type="email" placeholder="Email" />
          <p>Password</p>
          <input name="password" ref={passwordRef} type="password" placeholder="password" />
          <button type="submit">Submit</button>
        </form>
      </div>
    </div>
  );
}

export default Login;